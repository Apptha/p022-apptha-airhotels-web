<?php
/**
 * Apptha
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.apptha.com/LICENSE.txt
 *
 * ==============================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * ==============================================================
 * This package designed for Magento COMMUNITY edition
 * Apptha does not guarantee correct work of this extension
 * on any other Magento edition except Magento COMMUNITY edition.
 * Apptha does not provide extension support in case of
 * incorrect edition usage.
 * ==============================================================
 *
 * @category    Apptha
 * @package     Apptha_Airhotels
 * @version     0.2.9
 * @author      Apptha Team <developers@contus.in>
 * @copyright   Copyright (c) 2014 Apptha. (http://www.apptha.com)
 * @license     http://www.apptha.com/LICENSE.txt
 *
 */
class Apptha_Airhotels_Helper_Profile extends Mage_Core_Helper_Abstract {
	/**
  * Check whether the profile is completed or not 
  */
	public function profileCompletedOrNot(){
   	   /**
		* Get the customer Id from the session
		*/
    	$customerId = Mage::getSingleton('customer/session')->getCustomer()->getId();
   	   /**
		* Get the customer details from the customerphoto table
		*/
    	$customerPhotoCollection = Mage::getModel ( 'airhotels/customerphoto' )->load ( $customerId, 'customer_id' );
    	$hostCity		= 	$customerPhotoCollection->getCity();
    	$hostContact  	= 	$customerPhotoCollection->getContactNumber ();
    	$hostEmail		= 	$customerPhotoCollection->getEmailId ();
    	$customerCollection  = Mage::getModel ( 'customer/customer' )->load ( $customerId );
    	$hostGender		=	$customerCollection->getGender ();
    	$hostDob		=	$customerCollection->getDob ();
   		/**
		 * Check whether all the customer details has been filled or not
   		 */
    	if((!empty($hostContact))&&(!empty($hostGender))&&(!empty($hostDob))){
    		if((!empty($hostCity))&&(!empty($hostEmail))){
    			$returnUrl = Mage::getBaseUrl().'property/property/form';
    		}
    	}else{
	    	$returnUrl = Mage::getBaseUrl().'airhotels/index/profile/id/'.$customerId;
    	}
    return $returnUrl;
    }
}